## Add a Reset Camera button in XRInterface menu

When using Virtual Reality (VR) in ParaView, the menu now includes a button to reset the camera.
This brings the dataset(s) at the center of the physical world (the space where you can move).
